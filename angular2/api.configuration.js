import Session from './models/Session';
import User from './models/User';
import Post from './models/Post';
import Comment from './models/Comments';
import AboutMe from './models/AboutMe';
import Skill from './models/Skill';

export const apiConfiguration = {
    baseUrl: 'https://***/',
    endpoints: {

        session: {
            route: '/sessions/:id',
            model: Session,
            actions: {
                login: {
                    method: 'POST'
                }
            }
        },

        user: {
            route: '/users/:id',
            model: User,
            actions: {
                query: {
                    method: 'GET',
                    isArray: true,
                    headersForReading: ['X-Total-Count']
                }
            }
        },

        post: {
            route: '/posts/:id',
            model: Post,
            actions: {
                query: {
                    method: 'GET',
                    isArray: true,
                    headersForReading: ['X-Total-Count']
                }
            }
        },

        comment: {
            route: '/comments/:id',
            model: Comment,
            actions: {
                query: {
                    method: 'GET',
                    isArray: true,
                    headersForReading: ['X-Total-Count']
                }
            }
        },

        aboutMe: {
            route: '/about-me',
            model: AboutMe
        },

        skill: {
            route: '/skills/:id',
            model: Skill
        }

    }
};