import {Component, ChangeDetectionStrategy} from 'angular2/core';
import {ROUTER_DIRECTIVES, Router} from 'angular2/router';
import template from './menu.html';
import {UserManager} from '../../api/services/userManger';

@Component({
    selector: 'top-menu',
    template: template,
    directives: [ROUTER_DIRECTIVES],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopMenuComponent {
    static get parameters() {
        return [[UserManager], [Router]];
    }

    constructor(userManager, router) {
        this.userManager = userManager;
        this._router = router;
    }

    getLoggedIn() {
        return this.userManager.getLoggedIn();
    }

    logout() {
        this.userManager.logout();
        this._router.navigate(['Index']);
        return false;
    }
}
