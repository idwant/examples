import {Pipe} from 'angular2/core';

@Pipe({
    name: 'short_description'
})
export class ShortDescriptionPipe {
    transform(value, args) {
        if (value && value.length > 500) {
            return value.substring(0, 500) + '...';
        }
        else {
            return value;
        }
    }
}
