(function () {
    'use strict';
    angular
        .module('api')
        .config(config);

    /** @ngInject */
    function config(apiProvider, API_URL) {

        apiProvider.setBaseRoute(API_URL);

        apiProvider.endpoint('sessions')
            .route('sessions')
            .model('UserModel')
            .addHttpAction('POST', 'login');

        apiProvider.endpoint('users')
            .route('users')
            .model('UserModel');

        apiProvider.endpoint('summaryCustomer')
            .route('summary-customers/:id')
            .model('SummaryCustomerModel')
            .addHttpAction('GET', 'get', {isArray: false, params: {id: '@id'}})
            .addHttpAction('GET', 'query', {isArray: true})
            .addHttpAction('POST', 'create')
            .addHttpAction('POST', 'unplanned', {params: {id: 'unplanned'}});

        apiProvider.endpoint('summaryContact')
            .route('summary-contacts/:id/:sub')
            .model('SummaryContactModel')
            .addHttpAction('POST', 'create')
            .addHttpAction('POST', 'addPhones', {params: {id: '@id', sub: 'phones'}});

        apiProvider.endpoint('salesAgent')
            .route('customers/:id/sales-agent')
            .model('SalesAgentModel');

        apiProvider.endpoint('contacts')
            .route('summary-customers/:id/contacts')
            .model('SummaryContactModel')
            .addHttpAction('GET', 'query', {isArray: true});

        apiProvider.endpoint('searchContacts')
            .route('customers/search')
            .model('CustomerModel')
            .addHttpAction('POST', 'search', {isArray: true});

        apiProvider.endpoint('customerVisits')
            .route('customers/:id/visits')
            .model('VisitModel')
            .addHttpAction('GET', 'query', {isArray: true});

        apiProvider.endpoint('customerSchedule')
            .route('customers/:id/schedule')
            .model('CustomerScheduleModel')
            .addHttpAction('GET', 'get', {params: {id: '@id', year: '@year', month: '@month'}});

        apiProvider.endpoint('products')
            .route('summary-customers/:id/products')
            .model('ProductModel')
            .addHttpAction('GET', 'query', {isArray: true, params: {limit: '100'}})
            .addHttpAction('POST', 'search', {
                isArray: true,
                params: {id: '@id', limit: '100', name: null, number: null}
            });

        apiProvider.endpoint('productHierarchy')
            .route('product-hierarchy/:type')
            .model('ProductHierarchyModel')
            .addHttpAction('GET', 'query', {isArray: true});

        apiProvider.endpoint('calls')
            .route('calls/:id/:status')
            .model('CallModel')
            .addHttpAction('POST', 'startInbound', {params: {id: null, status: 'inbound'}})
            .addHttpAction('POST', 'start')
            .addHttpAction('PATCH', 'update', {params: {id: '@id'}})
            .addHttpAction('POST', 'finish', {params: {id: '@id', status: 'finish'}});

        apiProvider.endpoint('callResults')
            .route('call-results')
            .model('CallResultModel')
            .addHttpAction('GET', 'query', {cache: true, isArray: true});

        apiProvider.endpoint('visitResults')
            .route('visit-results')
            .model('VisitResultModel')
            .addHttpAction('GET', 'query', {cache: true, isArray: true});

        apiProvider.endpoint('salesOfficeSettings')
            .route('settings/sales-office/:customerId')
            .model('SalesOfficeSettingsModel')
            .addHttpAction('GET', 'get', {cache: true, params:{salesGroup : '@customerId', action: null }});

        apiProvider.endpoint('operatorKPI')
            .route('operator-kpi/:state')
            .model('OperatorKPIModel')
            .addHttpAction('GET', 'current', {isArray: false, params: {state: 'current'}});

        apiProvider.endpoint('orders')
            .route('orders/:action')
            .model('OrderModel')
            .addHttpAction('POST', 'search', {isArray: true, params: {action: 'search'}});

        apiProvider.endpoint('outboundOperatorCallLog')
            .route('admin/reports/outbound-operator-call-log')
            .model('TableModel')
            .addHttpAction('POST', 'search');

        apiProvider.endpoint('inboundOperatorCallLog')
            .route('admin/reports/inbound-operator-call-log')
            .model('TableModel')
            .addHttpAction('POST', 'search');

        apiProvider.endpoint('dashboardKPI')
            .route('dashboard-kpi')
            .model('DashboardKPIModel');
    }
})();


