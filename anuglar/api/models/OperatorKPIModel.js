(function () {
    'use strict';

    angular
        .module('api')
        .factory('OperatorKPIModel', OperatorKPIModel);

    /** @ngInject */
    function OperatorKPIModel(_baseModel, VisitResultModel, moment) {
        function OperatorKPI(resource) {
            resource = resource || {};

            this.planned_customer_count = resource.planned_customer_count || 0;
            this.answered_customer_count = resource.answered_customer_count || 0;
            this.backlog_customer_count = resource.backlog_customer_count || 0;
            this.called_customer_count = resource.called_customer_count || 0;
            this.closed_customer_count = resource.closed_customer_count || 0;
            this.planned_order_customer_count = resource.planned_order_customer_count || 0;
            this.unplanned_order_customer_count = resource.unplanned_order_customer_count || 0;
            this.operator_daily_order_sum= resource.operator_daily_order_sum|| 0;
            this.operator_monthly_order_sum= resource.operator_monthly_order_sum|| 0;
            this.operator_sales_plan = resource.operator_sales_plan || 0;
            this.agent_order_sum = resource.agent_order_sum || [];
            this.agent_sales_plan = resource.agent_sales_plan || [];
        }

        OperatorKPI.prototype = Object.create(_baseModel.prototype);
        OperatorKPI.prototype.constructor = OperatorKPI;

        //////////////////

        OperatorKPI.prototype.getRemainingCountDaysInMonth = function() {
            return +moment().daysInMonth()- +moment().format('D');
        };

        OperatorKPI.prototype.getMonthRunRate = function() {
            var runRate = (+this.operator_monthly_order_sum/ this.getRemainingCountDaysInMonth() / (+this.operator_sales_plan/+moment().daysInMonth())*100);
            return (runRate === Infinity) ? 0 : runRate || 0;
        };

        OperatorKPI.prototype.getSalesPlansWithSalesAgent = function() {
            var salesPlansWithSalesSalesAgent = [];
            var _tempSalesPlans = {};

            for(var i = 0; i < this.agent_order_sum.length; i++) {
                _tempSalesPlans[this.agent_order_sum[i].driver_id] = this.agent_order_sum[i];
            }

            for(i = 0; i < this.agent_sales_plan.length; i++) {
                _tempSalesPlans[this.agent_sales_plan[i].driver_id] =
                    angular.extend(this.agent_sales_plan[i], _tempSalesPlans[this.agent_sales_plan[i].driver_id]);
            }

            for(var salesPlan in _tempSalesPlans) {
                if (_tempSalesPlans.hasOwnProperty(salesPlan)) {

                    // Считаем runRate с тороговым представителем
                    _tempSalesPlans[salesPlan].runRate = {
                        x: salesPlan.monthly_order_sum / +moment().daysInMonth() - this.getRemainingCountDaysInMonth(),
                        y: salesPlan.sales_plan / +moment().daysInMonth()
                    };

                    salesPlansWithSalesSalesAgent.push(_tempSalesPlans[salesPlan]);
                }
            }

            return salesPlansWithSalesSalesAgent;
        };

        OperatorKPI.prototype.getOperatorDailySalesPlan = function() {
            return {
                x: this.operator_daily_order_sum,
                y: (this.operator_sales_plan * 0.4 - this.operator_monthly_order_sum) / this.getRemainingCountDaysInMonth()
            };
        };

        return OperatorKPI;
    }

})();
