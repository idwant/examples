(function () {
    'use strict';

    angular
        .module('api')
        .factory('KPIManager', KPIManager);

    /** @ngInject */
    function KPIManager($log, api) {

        var service = {
            getKPIForCurrentOperator: getKPIForCurrentOperator,
            getKpiForDashboard: getKpiForDashboard
        };

        //////////////////////////

        function getKPIForCurrentOperator() {
            return api.operatorKPI.current();
        }

        function getKpiForDashboard() {
            return api.dashboardKPI.get();
        }

        return service;
    }

})();
