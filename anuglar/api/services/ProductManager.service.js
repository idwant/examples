(function () {
    'use strict';

    angular
        .module('api')
        .factory('ProductManager', ProductManager);

    /** @ngInject */
    function ProductManager($log, api, $filter) {

        var service = {
            getProducts: getProducts,
            search: search,
            getProductsBrands: getProductsBrands,
            getProductsSizes: getProductsSizes
        };

        var cachedProducts = [];

        ///////////////

        function getProducts(summaryCustomerId, promosIds) {
            return api.products.query({id: summaryCustomerId, promos: promosIds.join(',')})
                .then(function(products){
                    cachedProducts = products;
                    return products;
                });
        }

        /**
         * тк объект dates и кол-во есть только у preferred products
         * то его кэшируем при первом запросе и пытаемся искать в нем совпадение
         * используется при поиске "обычных" продуктов
         */
        function _findProductDatesInCache(productId) {
            var filterResult = $filter('filter')(cachedProducts, {id: productId}, true);

            return filterResult.length ? filterResult[0].dates : {};
        }

        function search(summaryCustomerId, searchFormOriginal, promosIds) {
            var searchForm = angular.copy(searchFormOriginal);
            searchForm.volume = searchForm.volume || {};
            searchForm.category = searchForm.category || {};

            var form = {
                name: searchForm.text,
                product_brand: searchForm.category.code,
                size: searchForm.volume.code,
                promos: promosIds
            };

            return api.products.search(angular.extend({id: summaryCustomerId}, form))
                .then(function(products){
                    for(var i = 0; i < products.length; i++){
                        products[i].dates = _findProductDatesInCache(products[i].id);
                    }

                    return products;
                });
        }

        function getProductsBrands(summaryCustomerId) {
            return api.productHierarchy.query({type: "product-brands", summaryCustomerId: summaryCustomerId});
        }

        function getProductsSizes(code, summaryCustomerId) {
            return api.productHierarchy.query({type: "sizes", product_brand: code, summaryCustomerId: summaryCustomerId});
        }

        return service;
    }

})();
