(function () {
    'use strict';
    angular
        .module('components')
        .directive('clickOnce', clickOnce);

    function clickOnce() {

        var directive = {
            restrict: 'A',
            priority: -1,
            scope: {
                'ngClick': '&'
            },
            link: function (scope, element, attrs) {

                var disabled;
                var eventComplete = attrs.clickOnce;

                function onClick(evt) {
                    if (disabled) {
                        evt.preventDefault();
                        evt.stopImmediatePropagation();
                    } else {
                        disabled = true;
                    }
                }

                scope.$on(eventComplete, function () {
                    disabled = false;
                });
                element.bind('click', function (evt) {
                    onClick(evt)
                });
            }
        };

        return directive;
    }
})();