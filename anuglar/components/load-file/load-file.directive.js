(function () {
    'use strict';
    angular
        .module('components')
        .directive('loadFile', loadFile);

    function loadFile() {

        var directive = {
            restrict: 'EA',
            templateUrl: 'app/components/load-file/load-file.html',
            controller: LoadFileController,
            controllerAs: 'loadFile',
            bindToController: true,
            scope: {
                url: '@url',
                params: '=',
                successMessageTemplate: '@'
            }
        };

        return directive;
    }

    /** @ngInject */
    function LoadFileController(FileUploader, API_URL, $http, userConstants, $httpParamSerializerJQLike) {
        var vm = this;
        vm.loading = false;
        vm.success = false;
        vm.error = false;

        var initParams = {
            headers: {},
            url: API_URL + vm.url
        };

        initParams.headers[userConstants.AUTH_HEADER_KEY] = $http.defaults.headers.common[userConstants.AUTH_HEADER_KEY];

        ///////////////

        vm.uploader = new FileUploader(initParams);

        vm.repeat = function () {
            vm.loading = false;
            vm.success = false;
            vm.error = false;
        };

        vm.uploader.onAfterAddingFile = function (fileItem) {
            if (vm.loading) {
                return;
            }

            if (vm.params) {
                fileItem.url += '?'+$httpParamSerializerJQLike(vm.params);
            }

            fileItem.upload();
        };

        vm.uploader.onProgressItem = function (fileItem, progress) {
            vm.loading = true;
        };

        vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            vm.loading = false;
            vm.success = true;

            if (vm.successMessageTemplate) {
                vm.successMessage = vm.successMessageTemplate.replace(/%(.+?)%/gi, function (str, offset, s) {
                    str = response[offset];
                    return str;
                });
            }
        };

        vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
            vm.errorMessage = response.error;
            vm.loading = false;
            vm.error = true;
        };
    }

})();