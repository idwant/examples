/* global moment:false; */
(function () {
    'use strict';
    angular
        .module('decorators')
        .config(config);

    /** @ngInject */
    function config($provide) {
        $provide.decorator(
            "uibDatepickerDirective",
            function ($delegate) {
                var directive = $delegate[0],
                    link = directive.link;

                directive.$$isolateBindings['monthChanged'] = {
                    attrName: 'monthChanged',
                    mode: '=',
                    optional: true
                };

                directive.compile = function () {
                    return function (scope, element, attrs, ctrl) {
                        link.apply(this, arguments);
                        var controller = ctrl[0];

                        /**
                         * Переопределяем метод - тк используется one-way binding
                         * нужно перед обновлением view вызвать колбэк о смене даты
                         */
                        if (scope.monthChanged) {
                            controller.realRefreshView = controller.refreshView;
                            controller.refreshView = function () {
                                scope.monthChanged(moment(controller.activeDate).format('MM'), moment(controller.activeDate).format('YYYY'))
                                    .then(function () {
                                        controller.realRefreshView();
                                    });
                            };
                        }
                    }
                };

                return $delegate;
            }
        );
    }
})();