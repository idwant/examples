import template from './editor.html';
import Exercise from './../../../../api/models/Exercise';

class EditorController {
    constructor($log, $state, ExerciseManager, ExerciseTemplateManager, exceptionsFactory) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.exceptionsFactory = exceptionsFactory;
        this.ExerciseManager = ExerciseManager;
        this.ExerciseTemplateManager = ExerciseTemplateManager;
        this.templates = ExerciseTemplateManager.getAvailableTemplates();
        this.selectedComponent = null;
        this.savedResult = null;
    }

    audioLoaded(response = []) {
        let firstEl = response.shift() || {};
        this.audio = firstEl.id || null;
    }

    save(exercise) {
        return this.ExerciseManager.save(exercise)
            .then(() => {
                return this.savedResult = 'SUCCESS'
            })
            .catch((msg) => {
                this.savedResult = 'FAIL';
                return exceptionsFactory.generate('LOGIN_FAIL', msg);
            });
    }

    reload() {
        return this.$state.reload();
    }

}

export default {
    template: template,
    controller: EditorController
};