import routeConfig from './lesson.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let lessons = angular
    .module('lessons', [])
    .config(routeConfig)
    .component('editor', editorComponent)
    .component('list', listComponent)
    ;

export default lessons = lessons.name;