function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.lessons', {
            url: '/lessons',
            template: '<list class="lesson"></list>'
        });

    $stateProvider
        .state('base.lessons-edit', {
            url: '/lessons/edit/:id',
            template: '<editor class="lesson"></editor>'
        });
}

export default routeConfig;