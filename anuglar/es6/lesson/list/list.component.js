import template from './list.html';

class ListController {
    constructor($log, $scope, $q, Loader, CourseManager, UnitManager, LessonManager, LessonPartManager, ExerciseManager) {
        "ngInject";
        let promises = [];

        this.$log = $log;
        this.$scope = $scope;
        this.loader = Loader;
        this.CourseManager = CourseManager;
        this.UnitManager = UnitManager;
        this.LessonManager = LessonManager;
        this.LessonPartManager = LessonPartManager;
        this.ExerciseManager = ExerciseManager;

        this.courses = [];
        this.exercises = [];
        this.lessons = [];
        this.lessonParts = [];
        this.units = [];
        this.selectedCourse = null;
        this.selectedUnit = null;
        this.selectedLesson = null;
        this.selectedLessonPart = null;

        this.loader.start();
        promises.push(this.loadCourses());
        $q.all(promises)
            .then(() => {
                this._initWatchers();
                this.loader.complete();
            });
    }

    loadCourses() {
        return this.CourseManager.getCourses()
            .then((courses) => {
                return this.courses = courses;
            });
    }

    loadUnits(courseId) {
        return this.UnitManager.getUnits(courseId)
            .then((units) => {
                return this.units = units;
            });
    }

    loadLessons(courseId, unitId) {
        return this.LessonManager.getLessons(courseId, unitId)
            .then((lessons) => {
                return this.lessons = lessons;
            });
    }

    loadLessonParts(courseId, unitId, lessonId) {
        return this.LessonPartManager.getLessonParts(courseId, unitId, lessonId)
            .then((lessonParts) => {
                this.lessonParts = lessonParts;
            });
    }

    loadExercise(courseId, unitId, lessonId, lessonPartId) {
        return this.ExerciseManager.getExercises(courseId, unitId, lessonId, lessonPartId)
            .then((exercises) => {
                return this.exercises = exercises;
            });
    }

    _initWatchers() {
        // expressions
        let expressionForSelectedCourse = () => {
            return this.selectedCourse;
        };

        let expressionForSelectedUnit = () => {
            return this.selectedUnit;
        };

        let expressionForSelectedLesson = () => {
            return this.selectedLesson;
        };

        let expressionForSelectedLessonPart = () => {
            return this.selectedLessonPart;
        };

        // listeners
        let listenerForSelectedCourse = (newValue, oldValue) => {
            if (newValue != oldValue && this.selectedCourse) {
                return this.loadUnits(this.selectedCourse.id);
            }
        };

        let listenerForSelectedUnit = (newValue, oldValue) => {
            if (newValue != oldValue && this.selectedUnit) {
                return this.loadLessons(this.selectedCourse.id, this.selectedUnit.id);
            }
        };

        let listenerForSelectedLesson = (newValue, oldValue) => {
            if (newValue != oldValue && this.selectedLesson) {
                return this.loadLessonParts(this.selectedCourse.id, this.selectedUnit.id, this.selectedLesson.id);
            }
        };

        let listenerForSelectedLessonPart = (newValue, oldValue) => {
            if (newValue != oldValue && this.selectedLessonPart) {
                return this.loadExercise(this.selectedCourse.id, this.selectedUnit.id, this.selectedLesson.id, this.selectedLessonPart.id);
            }
        };

        this.$scope.$watch(expressionForSelectedCourse, listenerForSelectedCourse);
        this.$scope.$watch(expressionForSelectedUnit, listenerForSelectedUnit);
        this.$scope.$watch(expressionForSelectedLesson, listenerForSelectedLesson);
        this.$scope.$watch(expressionForSelectedLessonPart, listenerForSelectedLessonPart);
    }
}

export default {
    template: template,
    controller: ListController
};