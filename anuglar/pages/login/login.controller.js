(function () {
    'use strict';

    angular
        .module('login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($log, UserManager, toastr, pagesConstants, $location, exceptionsFactory) {
        var vm = this;
        vm.auth = auth;

        ////////////////

        function auth(email, password) {
            UserManager.login(email, password)
                .then(function (msg) {
                    toastr.success(msg);
                    return UserManager.current();
                })
                .then(function (user) {
                    $location.url(user.isAdmin() ? pagesConstants.MAIN_ADMIN :
                        user.isInbound() ? pagesConstants.MAIN_INBOUND : pagesConstants.MAIN);
                    return user;
                })
                .catch(function (msg) {
                    return exceptionsFactory.generate('LOGIN_FAIL', msg);
                });
        }
    }
})();
