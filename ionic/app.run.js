angular
    .module('app')
    .run(runBlock);


function runBlock($ionicPlatform, $cordovaSplashscreen, $rootScope, $cordovaNetwork,
                  $ionicUser, $ionicPush, $ionicPopup, api, $interval, $state, routes) {

    $rootScope.identifyUser = identifyUser;
    $rootScope.pushRegister = pushRegister;
    $rootScope.checkConnection = checkConnection;
    $rootScope.globalLoader = false;
    $rootScope.isOnline = true;

    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar && StatusBar && typeof StatusBar.style == 'function') {
            StatusBar.style(1);
        }

        screen.lockOrientation('portrait');

        // регистрируем пуши и пользователя
        if (!$rootScope.identified) {
            $rootScope.identifyUser();
        }

        $rootScope.pushRegister();
        $cordovaSplashscreen.hide();
    });
    $rootScope.$on('$cordovaPush:tokenReceived', tokenReceived);
    $rootScope.$on('$cordovaNetwork:online', setOnline);
    $rootScope.$on('$cordovaNetwork:offline', setOffline);

    ////////////////////////////////////////////////////////////
    function identifyUser() {
        var user = $ionicUser.get();
        if (!user.user_id) {
            user.user_id = $ionicUser.generateGUID();
        }

        angular.extend(user, {
            name: 'bate'
        });

        $ionicUser.identify(user).then(function () {
            $rootScope.identified = true;
        });
    }

    function pushRegister() {
        $ionicPush.register({
            canShowAlert: true,
            canSetBadge: true,
            canPlaySound: true,
            canRunActionsOnWake: true,
            onNotification: function (notification) {

                var buttons = [];

                if (notification.event == 'registered') {
                    return;
                }

                if (ionic.Platform.isAndroid()) {
                    notification = notification.payload.payload;
                }

                var gameId = false;
                if (notification.extra !== undefined && notification.extra.game !== undefined) {
                    gameId = notification.extra.game;
                } else if (notification.game) {
                    gameId = notification.game;
                }

                if (gameId) {
                    buttons = [
                        {text: 'Закрыть'},
                        {
                            text: '<b>Онлайн</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                $state.go(routes.matchCentre.state, {id: gameId});
                            }
                        }
                    ];
                } else {
                    buttons = [
                        {text: 'Закрыть'}
                    ];
                }

                var confirmPopup = $ionicPopup.alert({
                    cssClass: 'push-popup',
                    title: notification.title,
                    template: notification.body,
                    buttons: buttons
                });
                return false;
            }
        });
    }

    function tokenReceived(event, data) {
        $rootScope.token = data.token;
        var serverToken = new api.tokens({token: data.token});
        serverToken.$save();
    }

    function setOnline() {
        $rootScope.isOnline = true;
    }

    function setOffline() {
        $rootScope.isOnline = false;
        $rootScope.$broadcast("loader_hide");

        $rootScope.checkConnectionInterval = $interval($rootScope.checkConnection, 300);
    }

    function checkConnection() {
        if ($cordovaNetwork.isOnline()) {
            $rootScope.isOnline = true;
            $interval.cancel($rootScope.checkConnectionInterval);
        }
    }
}