angular
    .module('app.app-components')
    .config(dateDecorator);

/**
 * Декоратор для стандартного фильтра,
 * тк. js движок под ios не может конвертнуть стандартную для mysql дату
 */
function dateDecorator($provide){
    $provide.decorator('dateFilter', ['$delegate', function($delegate) {
        var srcFilter = $delegate;

        var extendsFilter = function() {
            if(arguments[0] != undefined && typeof arguments[0] == 'string' && arguments[0] != Number(arguments[0])) {
                var t = arguments[0].split(/[- :]/);
                arguments[0] = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
            }

            var g = srcFilter.apply(this, arguments);
            return g;
        };

        return extendsFilter;
    }])
}