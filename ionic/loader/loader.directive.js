angular
    .module('app.app-components')
    .directive('loader', loader);

function loader($ionicLoading, $rootScope) {
    return {
        restrict: 'E',
        replace: 'true',

        link: function (scope) {
            scope.$on("loader_show", function () {
                if($rootScope.globalLoader) {
                    $ionicLoading.show({
                        template: '<ion-spinner></ion-spinner>'
                    });
                }
            });
            scope.$on("loader_hide", function () {
                $ionicLoading.hide();
            });
        }
    };
}