angular
    .module('app.menu')
    .controller('MenuController', MenuController);

    function MenuController($scope, routes) {

        $scope.menu = [
            routes.feed,
            routes.club,
            routes.results,
            routes.calendar,
            routes.teams,
            routes.standings,
            routes.media,
            routes.stadium
        ];
    }