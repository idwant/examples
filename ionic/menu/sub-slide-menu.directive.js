angular
    .module('app.menu')
    .directive('subSlideMenu', subSlideMenu);

/**
 * Горизонтальная менюшка
 * которая скроллистя <-/->
 */
function subSlideMenu() {
    var directive = {
        link: link,
        templateUrl: 'menu/sub-slide-menu.html',
        restrict: 'EA',
        scope: {
            items: '=',
            selected: '=',
            callback: '&?'
        }
    };

    return directive;

    function link(scope, element, attrs) {

        scope.select = function (item){
            scope.selected = item;
            scope.callback({item:item});
        }
    }
}