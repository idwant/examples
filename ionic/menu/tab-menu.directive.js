angular
    .module('app.menu')
    .directive('tabMenu', tabMenu);

/**
 * Горизонтальная менюшка
 * табы
 */
function tabMenu() {
    var directive = {
        link: link,
        templateUrl: 'menu/tab-menu.html',
        restrict: 'EA',
        scope: {
            items: '=',
            selected: '=',
            callback: '&?'
        }
    };

    return directive;

    function link(scope, element, attrs) {

        scope.select = function (item){
            scope.selected = item;
            scope.callback({item:item});
        }
    }
}