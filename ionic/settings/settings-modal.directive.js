angular
    .module('app.settings')
    .directive('settingsModal', settingsModal);

function settingsModal($ionicModal, $cordovaInAppBrowser, $cordovaAppRate, $ionicPopup, $cordovaEmailComposer, emailUs) {
    var directive = {
        link: link,
        template: '<button class="button button-icon button-clear run-icon" ng-click="modal.show();"></button>',
        restrict: 'EA',
        scope: {
            video:  '=?video'
        }
    };

    return directive;

    function link(scope, element, attrs) {
        var confirmPopup = {};
        scope.emailUs = emailUs;
        scope.open = open;
        scope.openRate = openRate;
        scope.rating  = {
            rate: 0,
            max: 5
        };

        $ionicModal.fromTemplateUrl('settings/settings-modal.html', {
            animation: 'none',
            scope: scope,
            backdropClickToClose: false
        }).then(function (modal) {
            scope.modal = modal;
        });

        scope.$watch('rating.rate', function(oldv, newv) {
            if(oldv != newv){
                confirmPopup. close();
                if(scope.rating.rate > 3){
                    openRateAppStore();
                }
                else{
                    emailUs.open(scope.rating.rate);
                }
            }

        });

        /////////////
        function open(url){
            $cordovaInAppBrowser.open(url, '_system');
        }

        function openRate(){
             confirmPopup = $ionicPopup.alert({
                title: 'Мы будем рады если вы поставите оценку нашему приложению',
                template: '<rating ng-model="rating.rate" max="rating.max" on-hover="hoveringOver(value)"></rating>',
                scope: scope,
                buttons: [
                    { text: 'Закрыть' }
                ]
            });
        }
        // Открывает APP Store для оценки приложения
        function openRateAppStore(){
            return $cordovaAppRate.navigateToAppStore();
        }
    }
}