angular
    .module('app.video-player')
    .directive('videoPlayer', videoPlayer);

function videoPlayer($ionicModal, $sce, $rootScope) {
    var directive = {
        link: link,
        template: '<div ng-click="openModal();"><ng-transclude></ng-transclude></div>',
        restrict: 'EA',
        transclude: true,
        scope: {
            video:  '=?video'
        }
    };

    return directive;

    function link(scope, element, attrs) {
        var videoLink = '';
        var bateVideoHost = 'http://fcbate.by/';

        $rootScope.$on('$locationChangeSuccess', function(event, next, current) {
            scope.closeModal();
        });

        scope.$on('modal.hidden', function() {
            scope.src = $sce.trustAsResourceUrl('http://stopvideo/');
            screen.lockOrientation('portrait');
        });

        $ionicModal.fromTemplateUrl('video-player/video-player.html', {
            animation: 'none',
            scope: scope
        }).then(function (modal) {
            scope.modal = modal;
        });

        scope.openModal = function() {

            if(scope.video.is_youtube){
                videoLink = transformYoutubeFrameLink(scope.video.youtube_link);
            } else {

                // Если пришел относительный урл подставляем стандартный видеохост
                videoLink  = (scope.video.video.indexOf('http://') !== -1) ?
                      scope.video.video : bateVideoHost + scope.video.video;
            }

            scope.src =  $sce.trustAsResourceUrl(videoLink);
            scope.poster = scope.video.image;
            scope.modal.show();

            // Разблокируем что бы можно было повернуть
            screen.unlockOrientation();
        };

        scope.closeModal = function() {
            scope.src = $sce.trustAsResourceUrl('http://stopvideo/');
            scope.modal.hide();

            // лочим экран обратно
            screen.lockOrientation('portrait');
        };

        // Преобразовывает ютуб простую ютуб ссылку в годную для фрейма
        // если совпадение на youtu.be не было найдено - ничего не меняем
        function transformYoutubeFrameLink(link){
            var youTube = "https://youtu.be/";

            if(link.indexOf(youTube) !== -1) {
                return link.replace("https://youtu.be/", "https://www.youtube.com/embed/");
            }

            return link;
        }

    }
}